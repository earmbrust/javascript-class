(function (window) {
    var catElement = document.getElementsByClassName("cat")[0];
    var purrButton = catElement.getElementsByClassName("purr")[0];
    var scratchButton = catElement.getElementsByClassName("scratch")[0];
    var logArea = document.getElementById("log");

    var Animal = function () {};
    Animal.prototype.type = "Animal";
    Animal.prototype.scratch = function () {
        //perform action
        logArea.value += this.type + " scratches!\r\n";
    };

    var Cat = function () {};
    Cat.prototype = new Animal();
    Cat.prototype.type = "Cat";
    Cat.prototype.purr = function () {
        logArea.value += this.type + " purrs!\r\n";
    };

    //instanceof should provide true when comparing kitty instanceof Cat, as well as kitty instanceof Animal
    var kitty = new Cat();

    purrButton.addEventListener('click', function () {
        kitty.purr();
    });

    scratchButton.addEventListener('click', function () {
        kitty.scratch();
    });
}(window));