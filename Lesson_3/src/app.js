/**
 * Module dependencies.
 */

var host = "127.0.0.1";
var port = 3000;

var fs = require('fs');

var express = require('express');

var http = require('http');
var path = require('path');

var app = express();
var data_path = path.join(__dirname, 'data/data.json');
var data = JSON.parse(fs.readFileSync(data_path, 'utf8'));


var adjustData = function() {
    for (var movie_index in data.movies) {
        var movie = data.movies[movie_index];
        movie.links = {};
        movie.links.self = "http://" + host + ":" + port + "/movies/" + movie.id;
    }
    data.total = data.movies.length;
    data.links = {};
    data.links.self = "http://" + host + ":" + port + "/movies/";
};

var deleteMovie = function(id) {
    var retVal = false;
    for (var movie_index in data.movies) {
        var movie = data.movies[movie_index];
        if (id === movie.id) {
            data.movies.splice(movie_index, 1);
            retVal = true;
        }
    }
    persistData();
    return retVal;
};

var findDuplicate = function(id) {
    var retVal = -1;
    for (var movie_index in data.movies) {
        var movie = data.movies[movie_index];
        if (id === movie.id) {
            retVal = movie_index;
        }
    }
    return retVal;
};

var persistData = function() {
    adjustData();
    fs.writeFile(data_path, JSON.stringify(data), function(err) {
        if (err) throw err;
    });
};

app.set('port', process.env.PORT || port);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());



app.use('*', function(req, res, next) {
    //CORS FTW.
    res.header('Access-Control-Allow-Origin', '*');
    next();
});


app.get('/movies/', function(req, res) {
    adjustData();
    res.send(200, data);
});

app.get('/movies/:id', function(req, res) {
    adjustData();
    var id = req.params.id;
    var found = false;
    for (var movie_index in data.movies) {
        var movie = data.movies[movie_index];
        if (movie.id === id) {
            res.header('Content-Type', 'application/json');
            res.send(JSON.stringify(movie));
            found = true;
        }
    }
    if (!found) {
        res.header('Content-Type', 'text/plain');
        res.send(404);
    }
});

app.delete('/movies/:id', function(req, res) {
    if (deleteMovie(req.params.id) === true) {
        res.send(200);
    } else {
        res.send(404);
    }
});

app.put('/movies/:id', function (req, res) {
    if (findDuplicate(req.body.id) > 0) {
        update(req, res)
    } else {
        create(req, res);
    }
});

var update = function(req, res) {
    var retVal = false;
    var status = 404;
    var json_data = req.body;

    for (var movie_index in data.movies) {
        var movie = data.movies[movie_index];
        if (movie.id === req.params.id && json_data.id === req.params.id) {

            data.movies[movie_index] = json_data;
            retVal = true;
            status = 200
        } else {
            status = 409
        }
    }

    if (retVal === true) {
        persistData();
        res.send(status, json_data);
    } else {
        res.send(status);
    }
};

var create = function(req, res) {
    var retVal = 201;
    var json_data = req.body;
    if (findDuplicate(json_data.id) >= 0) {
        retVal = 403;
    } else {
        data.movies.push(json_data);
        data.total = data.movies.length;
        persistData();
    }

    res.send(retVal, json_data);
};

app.post('/movies/', create);



app.use('/', express.static(path.join(__dirname, 'public')));
app.use(express.errorHandler());



var server = http.createServer(app);
server.listen(app.get('port'), host, function() {
    console.log('Express server listening on port ' + app.get('port'));
});
