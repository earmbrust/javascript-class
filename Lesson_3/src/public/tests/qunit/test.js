(function ($) {
    var mockMovie = {
      "id": "TESTMOVIE",
      "title": "Test Movie",
      "year": 2014,
      "mpaa_rating": "R",
      "runtime": "120",
      "release_dates": {
        "theater": "2014-01-01"
      },
      "ratings": {
        "critics_score": -1,
        "audience_score": 95
      },
      "synopsis": "This is an example synopsis.",
      "posters": {
        "thumbnail": "thumbnail_url",
        "profile": "profile_url",
        "detailed": "deatiled_url",
        "original": "original_url"
      }
    };

    QUnit.config.reorder = false;

    asyncTest( "GET", function() {
        expect(5);
        $.ajax({
            url: "/movies/",
            success: function (data, statusText, xhr) {
                ok(!!data === true, "Got read response (GET)");
                equal(typeof data, "object", "Data response is an object");
                ok(data.hasOwnProperty('movies') === true, "Data has 'movies' property'");
                equal(statusText, "success", "Status text is correct ('success')");
                ok(xhr.status === 200, "Status code is correct (200)");
                start();
            },
            error: function () {
                start();
            }
        });
    });

    asyncTest("POST", function () {
        expect(10);
        $.ajax({
            url: "/movies/",
            type: "POST",
            data: mockMovie,
            dataType: 'json',
            success: function (data, statusText, xhr) {
                start();
                ok(!!data === true, "Got create response (POST)");
                equal(typeof data, "object", "Data response is an object");
                equal(statusText, "success", "Status text is correct ('success')");
                ok(xhr.status === 201, "Status code is correct (201)");
                stop();
            },
            error: function () {
                start();
            }
        });

        $.ajax({
            url: "/movies/TESTMOVIE",
            success: function (data, statusText, xhr) {
                start();
                ok(!!data === true, "Got validation response (GET)");
                equal(typeof data, "object", "Data response is an object");
                equal(statusText, "success", "Status text is correct ('success')");
                ok(xhr.status === 200, "Status code is correct (200)");
                equal(data.id, "TESTMOVIE", "Movie id matches ('TESTMOVIE')");
                equal(data.year, 2014, "Movie year matches (2014)");
            },
            error: function () {
                start();
            }
        });
    });


    asyncTest("PUT", function () {
        expect(11);
        var localMockMovie = Object.create(mockMovie);
        localMockMovie.year = 2015;
        $.ajax({
            url: "/movies/TESTMOVIE",
            type: "PUT",
            data: localMockMovie,
            dataType: 'json',
            success: function (data, statusText, xhr) {
                start();
                ok(!!data === true, "Got update response (PUT)");
                equal(typeof data, "object", "Data response is an object");
                equal(statusText, "success", "Status text is correct ('success')");
                ok(xhr.status === 200, "Status code is correct (200)");
                equal(data.year, 2015, "Date update matches (2015)");
                stop();
            },
            error: function () {
                start();
            }
        });

        $.ajax({
            url: "/movies/TESTMOVIE",
            success: function (data, statusText, xhr) {
                start();
                ok(!!data === true, "Got validation response (GET)");
                equal(typeof data, "object", "Data response is an object");
                equal(statusText, "success", "Status text is correct ('success')");
                ok(xhr.status === 200, "Status code is correct (200)");
                equal(data.id, "TESTMOVIE", "Movie id matches ('TESTMOVIE')");
                equal(data.year, 2015, "Movie year matches (2015)");
            },
            error: function () {
                start();
            }
        });
    });

    asyncTest("DELETE", function () {
        expect(8);
        $.ajax({
            url: "/movies/TESTMOVIE",
            type: "DELETE",
            success: function (data, statusText, xhr) {
                start();
                ok(!!data === true, "Got delete response (DELETE)");
                equal(typeof data, "string", "Data response is a string");
                equal(statusText, "success", "Status text is correct ('success')");
                ok(xhr.status === 200, "Status code is correct (200)");
                stop();
            },
            error: function () {
                start();
            }
        });
        $.ajax({
            url: "/movies/TESTMOVIE",
            error: function (xhr, statusText, response) {
                start();
                ok(!!response === true, "Got validation response (GET)");
                equal(typeof xhr, "object", "Data response is a object");
                equal(statusText, "error", "Status text is correct ('error')");
                ok(xhr.status === 404, "Status code is correct (404)");
            },
            success: function () {
                start();
            }
        });
    });
}(jQuery))
